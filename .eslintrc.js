module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
  ],
  parser: 'babel-eslint',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  rules: {
    'react/prop-types': 'off',
    'jsx-filename-extension': 'off',
    'linebreak-style': 'off',
    'no-undef': 'off',
    'max-len': 'off',
    'no-plusplus': 'off',
    'no-use-before-define': 'off',
    'react/jsx-no-undef': 'off',
    'react/jsx-filename-extension': 'off',
    'import/prefer-default-export': 'off',
    'import/no-unresolved': 'off',
    'react/react-in-jsx-scope': 'off',
    'import/order': 'off',
    'react/destructuring-assignment': 'off',
    'no-console': 'off',
    'no-this-in-sfc': 'off',
    'Unexpected token': 'off',
  },
};
