import React from 'react';
import './cars.styles.css';

export const Card = (props) => (
  <div className="card-container">
    <img alt="monster" src={`https://robohash.org/'${props.monster.id}?set=set1&size=300x300`} />
    <h2>
      {props.monster.name}
    </h2>
    <p>
      {props.monster.email }
    </p>
  </div>
);
